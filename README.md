# PGBlitz Standalone

Script will install ``python3-pip``,``bc`` and required dependancies for the python script

#### \*\*Script will move your old rclone config file (if you have one)\*\*
(old rclone file will be moved to ``/root/.config/rclone/rclone.conf.old``)
```
sudo apt install git -y && \
cd ~ && \
git clone https://gitlab.com/physk/pgblitz-standalone && \
cd pgblitz-standalone && \
chmod +x automated.sh && \
chmod +x pgblitz.py
```
if you have a setup like this (Most cloudbox users)
```tree /mnt/local/
/mnt/local/
└── Media
    ├── Movies
    ├── Music
    └── TV
```
when the script asks you for an upload directory use ``/mnt/local/Media``
this will upload files to ``TDRIVE:Movies``

if you wish to change the base dir on the Team Drive change so that it uploads to ``TDRIVE:Media/Movies``, change the following line in ``~/pgblitz-standalone/scripts/upload.sh``

Line 54: ``"${FILE}" "${REMOTE}:${FILEDIR}/${FILEBASE}"``

e.g. for a base Dir of ``Media`` change it to ``"${FILE}" "${REMOTE}:Media/${FILEDIR}/${FILEBASE}"``

(after you execute ``./automated.sh`` the file will be in ``/opt/appdata/pgblitz/upload.sh``)

From an elevated prompt (``sudo su``)
```
./automated.sh
```

if you get a ``Failed to move new rclone file, things won't upload until this is done`` error

```
mv /root/.config/rclone/rclone.conf /root/.config/rclone/rclone.conf.old
mv /opt/appdata/pgblitz/keys/rclone.conf /root/.config/rclone/rclone.conf
systemctl restart pgblitz
```
(the first command may fail if there is no rclone config file there in the first place)