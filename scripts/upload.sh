#!/bin/bash
#################################################################################
#  _____   _____ ____  _ _ _              ___   __ 
# |  __ \ / ____|  _ \| (_) |            |__ \ /_ |
# | |__) | |  __| |_) | |_| |_ ____ __   __ ) | | |
# |  ___/| | |_ |  _ <| | | __|_  / \ \ / // /  | |
# | |    | |__| | |_) | | | |_ / /   \ V // /_ _| |
# |_|     \_____|____/|_|_|\__/___|   \_/|____(_)_|
#
#################################################################################

# Logging Function
function log()
{
    echo "[PGBlitz] $@"
}

downloadpath=$(cat /opt/appdata/pgblitz/vars/server.hd.path)
IFS=$'\n'

FILE=$1
GDSA=$2
log "[Upload] Upload started for $FILE using $GDSA"

STARTTIME=`date +%s`
FILESTER=`echo $FILE | sed "s#${downloadpath}##g"`
FILEBASE=`basename $FILE`
FILEDIR=`dirname $FILE | sed "s#${downloadpath}/##g"`

JSONFILE=/opt/appdata/pgblitz/json/$FILEBASE.json

# add to file lock to stop another process being spawned while file is moving
echo "lock" > $FILE.lck

#get Human readable filesize
HRFILESIZE=`ls -lsah $FILE | awk '{print $6}'`

REMOTE=$GDSA

log "[Upload] Uploading $FILE to $REMOTE"
LOGFILE=/opt/appdata/pgblitz/logs/$FILEBASE.log

#create and chmod the log file so that webui can read it
touch $LOGFILE
chmod 777 $LOGFILE

#update json file for PGBlitz GUI
echo "{\"filedir\": \"$FILEDIR\",\"filebase\": \"$FILEBASE\",\"filesize\": \"$HRFILESIZE\",\"status\": \"uploading\",\"logfile\": \"/logs/$FILEBASE.log\",\"gdsa\": \"$GDSA\"}" > $JSONFILE
log "[Upload] Starting Upload"
rclone moveto --tpslimit 6 --checkers=20 \
      --config /root/.config/rclone/rclone.conf \
      --log-file=$LOGFILE --log-level INFO --stats 2s \
      --drive-chunk-size=32M \
      "${FILE}" "${REMOTE}:${FILEDIR}/${FILEBASE}"

ENDTIME=`date +%s`
#update json file for PGBlitz GUI
echo "{\"filedir\": \"$FILEDIR\",\"filebase\": \"$FILEBASE\",\"filesize\": \"$HRFILESIZE\",\"status\": \"done\",\"gdsa\": \"$GDSA\",\"starttime\": \"$STARTTIME\",\"endtime\": \"$ENDTIME\"}" > $JSONFILE

log "[Upload] Upload complete for $FILE, Cleaning up"

#cleanup
#remove file lock
rm -f $FILE.lck
rm -f $LOGFILE
rm -f /opt/appdata/pgblitz/pid/$FILEBASE.trans
find ${downloadpath} -mindepth 2 -type d -empty -delete
sleep 60
rm -f $JSONFILE