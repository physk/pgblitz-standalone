#!/bin/bash
echo "INFO - Creating needed Directories"
mkdir -p /opt/appdata/pgblitz/vars/
mkdir -p /opt/appdata/pgblitz/keys/

echo "INFO - Installing Requirements"
sleep 5
apt install bc python3-pip -y
#pip3 install -r requirements.txt

echo ""
echo "NOTE: Pay Attention! USE the ACCOUNT of Your Business G-Suite!"
echo "Failing to do so will [RESULT] in Script Failure!"
echo 'INFO - RUNNING Auto SA Tool by Storm'
sleep 5

python3 pgblitz.py
if [ $? == 1 ]; then
    echo ""
    read -n 1 -s -r -p "PGBlitz.Py Could Not Execute due to an Internal Error! Press [Any Key] to Continue"
    exit
fi

if [ -e /root/.config/rclone/rclone.config ]; then
    mv /root/.config/rclone/rclone.conf /root/.config/rclone/rclone.conf.old
    echo ""
    read -n 1 -s -r -p "You're old rclone config has been moved to /root/.config/rclone/rclone.conf.old - Press [Any Key] to Continue"
fi
mkdir /root/.config/rclone
mv /opt/appdata/pgblitz/keys/rclone.conf /root/.config/rclone/rclone.conf
if [ -e /opt/appdata/pgblitz/keys/rclone.conf ]; then
    echo "Failed to move new rclone file, things won't upload until this is done. the file is currently stored in:"
    echo "/opt/appdata/pgblitz/keys/rclone.conf"
else
    echo "Successfull: Moved new rclone file to /root/.config/rclone/rclone.conf"
fi

read -p "Enter your upload directory: " SERVER_PATH
echo ${SERVER_PATH} > /opt/appdata/pgblitz/vars/server.hd.path
echo "Copying PGBlitz files"
cp scripts/pgblitz.sh /opt/appdata/pgblitz/
cp scripts/upload.sh /opt/appdata/pgblitz/
echo "Setting permissions on PGBlitz Files"
chmod +x /opt/appdata/pgblitz/*.sh
echo "Copying Service files"
cp scripts/pgblitz.service /etc/systemd/system/
echo "Setting permissions on service files"
chmod +x /etc/systemd/system/pgblitz.service
echo "Enabling service"
systemctl enable pgblitz.service
echo "Starting Service"
systemctl start pgblitz.service

read -n 1 -s -r -p "PGBlitz Deployed! Press [ANY KEY] to Continue"
echo ""
